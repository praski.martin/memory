var numbers = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8];
var draw;
var i;
var number = "";
var openCards = 0;
var numberOfMoves = 0;
var uncoveredCards = 0;

$(document).ready(function() {
    for (i = 0; i < 16; i++) {
        drawCards(i);
    }
    $('body').on('click', '.to-open', showNumber);
});

function drawCards(id) {
    draw = Math.floor(Math.random() * numbers.length);
    number = numbers[draw];

    var button = $("#"+ id);
    button.data('number', number);

    numbers.splice(draw, 1);
}

function showNumber() {
    numberOfMoves++;
    $(action).html(numberOfMoves);
    openCards++;
    var button = $(this);
    var number = button.data('number');
    button.html(number);
    button.addClass("open");
    if (openCards === 2) {
        var toOpenTheCard = $(".to-open");
        for (var i = 0; i < toOpenTheCard.length; i++){
            toOpenTheCard[i].disabled=true;
        }
        setTimeout(function () {
            var toOpenTheCard = $(".to-open");

            for (var j = 0; j < toOpenTheCard.length; j++){
                toOpenTheCard[j].disabled=false;

            }
            var open = $(".open");
            var openNumbers = [];

            open.each(function () {
                var number = $(this).data('number');
                openNumbers.push(number);
            });

            if (openNumbers[0] === openNumbers[1]) {
                open.each(function () {
                    var button = $(this);
                    button.removeClass('open');
                    button.removeClass('to-open');
                    button.prop('disabled', true);
                    button.addClass('blue');
                    uncoveredCards++;
                    if (uncoveredCards === 16){
                        var pushbutton =document.getElementById("swap");
                        pushbutton.style.display = 'block';
                    }
                });
            } else {
                for (var i = 0; i < open.length; i++) {
                    var button = open[i];
                    button.innerHTML = "?";
                    button.classList.remove('open');
                }
            }
            openCards = 0;
        }, 500)
    }
}
